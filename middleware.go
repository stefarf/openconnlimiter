package openconnlimiter

import (
	"net/http"
	"sync"

	"github.com/labstack/echo/v4"
)

func Middleware(limitConnection int) echo.MiddlewareFunc {
	var mut sync.Mutex
	openConnection := 0

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			mut.Lock()
			if openConnection >= limitConnection {
				mut.Unlock()
				return c.String(http.StatusServiceUnavailable, "Rejected: Open connection limit")
			}
			openConnection++
			mut.Unlock()

			defer func() {
				mut.Lock()
				openConnection--
				mut.Unlock()
			}()
			return next(c)
		}
	}
}
