package openconnlimiter

import (
	"reflect"
	"testing"
)

func assertDeepEqual(t *testing.T, got, want any, message string) {
	if !reflect.DeepEqual(got, want) {
		t.Errorf("%s: got %v, want %v\n", message, got, want)
	}
}
