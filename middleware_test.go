package openconnlimiter

import (
	"log"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
)

func TestMiddleware(t *testing.T) {
	limitConnection := 5
	totalConnection := limitConnection * 50
	timeUnit := time.Millisecond * 10

	limiterMiddleware := Middleware(limitConnection)

	var result struct {
		sync.Mutex
		runNext int
		codeMap map[int]int
	}
	result.codeMap = map[int]int{}
	next := func(c echo.Context) error {
		result.Lock()
		result.runNext++
		result.Unlock()
		time.Sleep(timeUnit)
		return nil
	}

	runLimiter := func() {
		r := httptest.NewRequest("GET", "/any/url", nil)
		w := httptest.NewRecorder()
		e := echo.New()
		c := e.NewContext(r, w)

		f := limiterMiddleware(next)
		err := f(c)
		if err != nil {
			log.Fatal(err)
		}

		result.Lock()
		result.codeMap[w.Code]++
		result.Unlock()
	}

	var wg sync.WaitGroup
	for i := 0; i < totalConnection; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			runLimiter()
		}()
	}
	wg.Wait()

	assertDeepEqual(t, result.runNext, limitConnection, "runNext")
	assertDeepEqual(t, result.codeMap, map[int]int{
		http.StatusOK:                 limitConnection,
		http.StatusServiceUnavailable: totalConnection - limitConnection,
	}, "codeMap")

	runLimiter()

	assertDeepEqual(t, result.runNext, limitConnection+1, "runNext")
	assertDeepEqual(t, result.codeMap, map[int]int{
		http.StatusOK:                 limitConnection + 1,
		http.StatusServiceUnavailable: totalConnection - limitConnection,
	}, "codeMap")
}
