#!/usr/bin/env bash
VERSION=$1
[ -z "$VERSION" ] && echo "VERSION variable is not defined" && exit 1
GOPROXY=proxy.golang.org go list -m gitlab.com/stefarf/openconnlimiter@$VERSION
